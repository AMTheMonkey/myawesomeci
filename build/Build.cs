using Nuke.Common;
using Nuke.Common.Git;
using Nuke.Common.IO;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.Docker;
using Nuke.Common.Tools.DotNet;
using Nuke.Common.Tools.GitVersion;
using Nuke.Common.Tools.SonarScanner;
using Serilog;

class Build : NukeBuild
{
    /// Support plugins are available for:
    ///   - JetBrains ReSharper        https://nuke.build/resharper
    ///   - JetBrains Rider            https://nuke.build/rider
    ///   - Microsoft VisualStudio     https://nuke.build/visualstudio
    ///   - Microsoft VSCode           https://nuke.build/vscode

    [Parameter] [Secret] readonly string GitUserName;
    [Parameter] [Secret] readonly string GitUserMail;
    [Parameter] [Secret] readonly string GitUrlPushTags;
    [Parameter] [Secret] readonly string SonarToken;
    [Parameter] [Secret] readonly string SonarProjectKey;
    [Parameter] [Secret] readonly string SonarHostUrl;
    [Parameter] [Secret] readonly string SonarOrganization;
    [Parameter] [Secret] readonly string DockerRegistryUrl;
    [Parameter] [Secret] readonly string DockerRegistryUser;
    [Parameter] [Secret] readonly string DockerRegistryToken;
    

    AbsolutePath DockerFile = RootDirectory / "MyAwesomeSolution" / "Dockerfile";
    
    public static int Main () => Execute<Build>(x => x.Compile);

    [Parameter("Configuration to build - Default is 'Debug' (local) or 'Release' (server)")]
    readonly Configuration Configuration = IsLocalBuild ? Configuration.Debug : Configuration.Release;

    [PathVariable]
    readonly Tool Git;
    
    [GitRepository] 
    readonly GitRepository Repository;
    
    [GitVersion]
    readonly GitVersion GitVersion;

    Target PrintGitVersion => _ => _
        .Executes(() =>
        {
            Log.Information("GitVersion = {Value}", GitVersion.FullSemVer);
        });
    Target SonarScannerBegin => _ => _
        .Before(Clean)
        .Executes(() =>
        {
            SonarScannerTasks.SonarScannerBegin(_ =>
                _.SetToken($"{SonarToken}")
                    .SetProjectKey(SonarProjectKey)
                    .SetOrganization(SonarOrganization)
                    .SetServer($"{SonarHostUrl}"));
        });
    
    Target Clean => _ => _
        .Before(Restore)
        .Executes(() =>
        {
            DotNetTasks.DotNetClean();
        });

    Target Restore => _ => _
        .Executes(() =>
        {
            DotNetTasks.DotNetRestore();
        });

    Target Compile => _ => _
        .DependsOn(Restore)
        .Executes(() =>
        {
            DotNetTasks.DotNetBuild(_ => _.SetNoIncremental(true));
        });
    
    Target SonarScannerEnd => _ => _
        .DependsOn(Compile)
        .Executes(() =>
        {
            SonarScannerTasks.SonarScannerEnd(_ =>
                _.SetToken($"{SonarToken}"));
        });
    
    
    Target Tag => _ => _
        .DependsOn(SonarScannerEnd)
        .Executes(() =>
        {
            Git($"config --global user.email \"{GitUserMail}\"");
            Git($"config --global user.name \"{GitUserName}\"");

            Git($"tag -a {GitVersion.FullSemVer} -m \"Setting git tag on commit to '{GitVersion.FullSemVer}'\"");
            Git($"push --tags {GitUrlPushTags} HEAD:master");
        });

    Target DockerRegistry => _ => _
        .Requires(() => Repository.IsOnMainOrMasterBranch())
        .Executes(() =>
        {
            Log.Information("Beginning of target DockerRegistry");
            DockerTasks.DockerLogin(_ => _.SetUsername($"{DockerRegistryUser}")
                .SetServer($"{DockerRegistryUrl}")
                .SetPassword(DockerRegistryToken));

            DockerTasks.DockerBuild(_ => _.SetPath($"{DockerRegistryUrl}/amthemonkey/myawesomeci")
                .SetTag(GitVersion.FullSemVer)
                .SetFile(DockerFile));

            DockerTasks.DockerPush(_ => _.SetName($"{DockerRegistryUrl}/amthemonkey/myawesomeci:{GitVersion.FullSemVer}"));
            Log.Information("End of target DockerRegistry");
        });

}
